<?php

namespace Drupal\simple_entity_diff\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Url;

/**
 * Simple entity diff controller.
 *
 * @package Drupal\simple_entity_diff\Controller
 */
class SimpleEntityDiffController extends ControllerBase {

  /**
   * Compare two revisions of the same entity.
   *
   * @param string $entity_type
   *   Machine name of the entity.
   * @param string $rev_a
   *   Revision A number.
   * @param string $rev_b
   *   Revision B number.
   *
   * @return array
   *   Render build.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function viewDiff(string $entity_type, string $rev_a, string $rev_b) {
    $loaded_rev_a = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->loadRevision($rev_a);

    $rev_a_data = $loaded_rev_a->toArray();
    $loaded_rev_b = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->loadRevision($rev_b);
    $rev_b_data = $loaded_rev_b->toArray();

    $field_list = array_unique(array_merge_recursive(array_keys($rev_a_data), array_keys($rev_b_data)));
    $build = [
      '#theme' => 'simple_entity_diff',
      '#rev_a' => $rev_a_data,
      '#rev_a_number' => $rev_a,
      '#rev_b' => $rev_b_data,
      '#rev_b_number' => $rev_b,
      '#field_list' => $field_list,
    ];

    return $build;
  }

  /**
   * List entities.
   *
   * @return array
   *   Render build.
   */
  public function listEntityTypes() {
    $allEntities = \Drupal::entityTypeManager()->getDefinitions();
    $contentEntityList = [];
    foreach ($allEntities as $entityConfig) {
      /** @var \Drupal\Core\Entity\ContentEntityType $entityConfig */
      if ($entityConfig->getGroup() === 'content' && $entityConfig->getPermissionGranularity() === 'entity_type') {
        $contentEntityList[] = [
          'id'  => $entityConfig->id(),
          'class' => $entityConfig->getClass(),
        ];
      }
    }
    return [
      '#theme'  => 'simple_entity_diff_entity_type_list',
      '#entity_type_list' => $contentEntityList,
    ];
  }

  /**
   * List revisions based on the entity.
   *
   * @param string $entity_type
   *   The entity which revisions we list.
   *
   * @return array
   *   Render build.
   */
  public function listEntities(string $entity_type) {
    $list = [];

    $entityList = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->loadMultiple();

    foreach ($entityList as $entity) {
      /** @var \Drupal\Core\Entity\ContentEntityBase $entity */
      $type = $entity->getEntityType();
      $list[] = [
        'id'  => $entity->id(),
        'name' => $entity->get('name')->value,
        'type'  => $type->id(),
      ];
    }

    return [
      '#theme'  => 'simple_entity_diff_entity_list',
      '#entity_list' => $list,
    ];
  }

  public function listEntityRevisions(string $entity_type, string $entity_id) {
    $revision_list = [];
    $entity = $this->entityTypeManager()
      ->getStorage($entity_type)
      ->load($entity_id);
    $type = $entity->getEntityType();

    $revisionList = \Drupal::entityTypeManager()->getStorage($type->id())->revisionIds($entity);

    return [
      '#theme'  => 'simple_entity_diff_entity_revision_list',
      '#entity_revision_list' => $revisionList,
    ];
  }

}
